This repo contains lots of useful binaries compiled for both the satellite
and the nonflight ground computers. Compiled so far:

(Last updated 1 Oct 2017)

Program (architecture)
- at, atq, batch, atrm (ARM)
- netcat (ARM)
- predict (x86_64)
